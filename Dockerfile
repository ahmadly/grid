FROM rust:latest
WORKDIR /opt/grid
RUN git clone -b main https://github.com/hyperledger/grid.git /opt/grid
RUN apt update && apt install -y libzmq3-dev protobuf-compiler && rm -rf /var/lib/apt/lists/*
RUN cargo fetch
RUN cargo build --release

FROM alpine:latest
COPY --from=0 /opt/grid/target/release/grid-schema-tp /sbin
COPY --from=0 /opt/grid/target/release/grid-track-and-trace-tp /sbin
COPY --from=0 /opt/grid/target/release/gridd /sbin
COPY --from=0 /opt/grid/target/release/grid-product-tp /sbin
COPY --from=0 /opt/grid/target/release/grid /sbin
COPY --from=0 /opt/grid/target/release/griddle /sbin
COPY --from=0 /opt/grid/target/release/grid-location-tp /sbin
COPY --from=0 /opt/grid/target/release/grid-pike-tp /sbin
COPY --from=0 /opt/grid/target/release/grid-purchase-order-tp /sbin
CMD bash
